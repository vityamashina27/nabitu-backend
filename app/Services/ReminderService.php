<?php

namespace App\Services;

use App\Models\Reminder;

class ReminderService {
    function create(array $data) : Reminder {
        return Reminder::create($data);
    }

    function update(Reminder $reminder, array $updatedData) : Reminder {
        $reminder->fill($updatedData);
        $reminder->save();
        return $reminder;
    }
}
