<?php

namespace App\Http\Requests\Reminder;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class ReminderStoreRequest extends FormRequest {
    public function rules() : array {
        return [
            'title'       => 'required|max:255',
            'description' => 'required|max:255',
            'remind_at'   => 'required|numeric',
            'event_at'    => 'required|numeric',
        ];
    }

    public function authorize(): bool {
        return true;
    }

    public function failedValidation(Validator $validator): JsonResponse {
        return response()->json([
            'ok'  => false,
            'err' => 'ERR_BAD_REQUEST',
            'msg' => $validator->errors()->first()
        ]);
    }
}
