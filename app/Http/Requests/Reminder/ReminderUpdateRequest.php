<?php

namespace App\Http\Requests\Reminder;

use Illuminate\Foundation\Http\FormRequest;

class ReminderUpdateRequest extends FormRequest {
    public function rules() : array {
        return [
            'title'       => 'required|max:255',
            'description' => 'required|max:255',
            'remind_at'   => 'required|numeric',
            'event_at'    => 'required|numeric',
        ];
    }

    public function authorize(): bool {
        return true;
    }
}
