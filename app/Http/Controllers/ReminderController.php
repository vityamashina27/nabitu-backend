<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reminder\ReminderStoreRequest;
use App\Http\Requests\Reminder\ReminderUpdateRequest;
use App\Models\Reminder;
use App\Services\ReminderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ReminderController extends Controller
{
    const ERROR_INTERNAL = 'ERR_INTERNAL_ERROR';
    const ERROR_BAD_REQUEST = 'ERR_BAD_REQUEST';

    /**
     * Retrieves a list of reminders from the database based on the provided request parameters and returns a JSON response with the reminders and the limit.
     *
     * @param Request $request The request object containing the limit parameter.
     * @return JsonResponse A JSON response containing the retrieved reminders and the limit.
     */
    public function index(Request $request): JsonResponse
    {
        $reminderLimit = $request->input('limit', 10);
        $reminders = Reminder::select('id', 'title', 'description', 'remind_at', 'event_at')
            ->limit($reminderLimit)
            ->orderBy('remind_at')
            ->get();

        /** Convert it from a string format to a Unix timestamp format. */
        foreach ($reminders as $reminder) {
            $reminder->event_at = (string)strtotime($reminder->event_at);
            $reminder->remind_at = (string)strtotime($reminder->remind_at);
        }

        return response()->json(
            [
                'ok' => true,
                'data' => [
                    'reminders' => $reminders,
                    'limit' => $reminderLimit,
                ],
            ],
            200,
        );
    }

    /**
     * Store a new reminder.
     *
     * @param ReminderStoreRequest $request The request object.
     * @param ReminderService $reminderService The reminder service.
     * @return JsonResponse The JSON response with the created reminder's data.
     */
    public function store(ReminderStoreRequest $request, ReminderService $reminderService): JsonResponse
    {
        try {
            Log::info('[STORE-REMINDER] New request for reminder: ' . json_encode($request->all(), true));
            $reminderData = $request->validated();

            Log::info('[STORE-REMINDER] Saving data into database: ' . json_encode($reminderData));
            $reminder = $reminderService->create($reminderData);

            Log::info('[STORE-REMINDER] 200 Reminder is saved : ' . json_encode($reminder, true));
            return Response::json([
                'ok' => true,
                'data' => [
                    'id' => $reminder->id,
                    'title' => $reminder->title,
                    'description' => $reminder->description,
                    'remind_at' => (string) $reminder->remind_at,
                    'event_at' => (string) $reminder->event_at,
                ],
            ]);
        } catch (ValidationException $e) {
            Log::info('[STORE-REMINDER] 400 Reminder got bad request : ' . $e->getMessage());
            return Response::json(
                [
                    'ok' => false,
                    'err' => self::ERROR_BAD_REQUEST,
                    'msg' => $e->getMessage(),
                ],
                400,
            );
        } catch (\Throwable $th) {
            report($th);
            Log::info('[STORE-REMINDER] 500 Server has error : ' . $th->getMessage());
            return Response::json(
                [
                    'ok' => false,
                    'err' => self::ERROR_INTERNAL,
                    'msg' => $th->getMessage(),
                ],
                500,
            );
        }
    }

    /**
     * Show method
     *
     * Returns a JSON response with the details of a specific reminder.
     *
     * @param Reminder $reminder An instance of the Reminder model representing a specific reminder.
     * @return JsonResponse A JSON response with the details of the reminder.
     *
     * @throws \Throwable If an exception occurs during the process.
     */
    public function show(Reminder $reminder): JsonResponse
    {
        try {
            return Response::json([
                'ok' => true,
                'data' => [
                    'id' => $reminder->id,
                    'title' => $reminder->title,
                    'description' => $reminder->description,
                    'remind_at' => (string) $reminder->remind_at,
                    'event_at' => (string) $reminder->event_at,
                ],
            ]);
        } catch (\Throwable $th) {
            report($th);
            return Response::json(
                [
                    'ok' => false,
                    'err' => self::ERROR_INTERNAL,
                    'msg' => $th->getMessage(),
                ],
                500,
            );
        }
    }

    /**
     * Update a reminder based on the provided request data.
     *
     * @param ReminderUpdateRequest $request The request data.
     * @param Reminder $reminder The reminder to be updated.
     * @param ReminderService $reminderService The service used to update the reminder.
     * @return \Illuminate\Http\JsonResponse The JSON response with the updated reminder's data or an error message.
     */
    public function update(Reminder $reminder,Request $request,  ReminderService $reminderService)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required|string|max:255',
            'description' => 'nullable|string',
            'remind_at'   => 'required|numeric',
            'event_at'    => 'required|numeric',
        ], [
            'numeric' => 'Please provide a valid unix time for :attribute',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'err' => self::ERROR_BAD_REQUEST,
                'msg' => $validator->errors()->first(), // Contains custom error messages
            ], 400);
        }

        try {
            $reminderData = $validator->validated();
            Log::info('[UPDATE-REMINDER] Updating data reminder into database: ' . json_encode($reminderData));
            $newReminder = $reminderService->update($reminder, $reminderData);
            Log::info('[UPDATE-REMINDER] Update success with result: ' . json_encode($newReminder));

            return Response::json([
                'ok' => true,
                'data' => [
                    'id' => $newReminder->id,
                    'title' => $newReminder->title,
                    'description' => $newReminder->description,
                    'remind_at' => $newReminder->remind_at,
                    'event_at' => $newReminder->event_at,
                ],
            ]);
        } catch (ValidationException $e) {
            return Response::json(
                [
                    'ok' => false,
                    'err' => self::ERROR_BAD_REQUEST,
                    'msg' => $e->getMessage(),
                ],
                400,
            );
        } catch (\Throwable $th) {
            report($th);
            return Response::json(
                [
                    'ok' => false,
                    'err' => self::ERROR_INTERNAL,
                    'msg' => $th->getMessage(),
                ],
                500,
            );
        }
    }

    /**
     * Deletes a specific reminder from the database and returns a JSON response indicating success or failure.
     *
     * @param Reminder $reminder An instance of the Reminder model representing the reminder to be deleted.
     * @return \Illuminate\Http\JsonResponse A JSON response indicating success or failure of the deletion.
     */
    public function destroy(Reminder $reminder)
    {
        try {
            $reminder->delete();
            return Response::json([
                'ok' => true,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return Response::json(
                [
                    'ok' => false,
                    'err' => self::ERROR_INTERNAL,
                    'msg' => $th->getMessage(),
                ],
                500,
            );
        }
    }
}
