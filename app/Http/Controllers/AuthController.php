<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PHPOpenSourceSaver\JWTAuth\Exceptions\JWTException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\TokenExpiredException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\TokenInvalidException;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    function login(): JsonResponse
    {
        $credentials = request(['email', 'password']);

        if (
            !($token = auth()
                ->setTTL(1)
                ->attempt($credentials))
        ) {
            return response()->json([
                "ok" => false,
                "err" => "ERR_INVALID_CREDS",
                "msg" => "incorrect username or password",
            ], 401);
        }

        $refreshToken = JWTAuth::setToken(auth()->setTTL(2030)->attempt($credentials))->refresh();

        $user = User::select('id', 'email', 'name')->where([
            'email' => request()->email,
        ])->first();
        $data = [
            'user' => $user,
            'access_token' => $token,
            'refresh_token' => $refreshToken,
        ];

        return response()->json([
            'ok' => true,
            'data' => $data,
        ]);
    }

    function refresh(Request $request): JsonResponse
    {
        $refreshToken = $request->token;
        Log::info('[REFRESH TOKEN] Trying to refresh token '. json_encode($request->all()));

        $refreshTokenExceptionMessage = response()->json([
            "ok" => false,
            "err" => "ERR_INVALID_REFRESH_TOKEN",
            "msg" => "invalid refresh token",
        ], 401);


        /** Checking if refresh token is valid */
        try {
            JWTAuth::parseToken($refreshToken)->authenticate();
        } catch (TokenExpiredException $e) {
            return $refreshTokenExceptionMessage;
        } catch (TokenInvalidException $e) {
            return $refreshTokenExceptionMessage;
        } catch (JWTException $e) {
            return $refreshTokenExceptionMessage;
        }

        try {
            $userId = auth()->user()->id;
            $token = auth()->setTTL(1)->tokenById($userId);
            $data = [
                'access_token' => $token,
            ];

            return response()->json([
                'ok' => true,
                'data' => $data,
            ]);
        } catch (AuthenticationException $e) {
            return json_encode($e);
        }
    }

    function register(Request $request): JsonResponse
    {
        return response()->json();
    }

    function respondWithToken($token, $refreshToken = null): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'refresh_token' => $refreshToken,
            'token_type' => 'bearer',
            'expires_in' => JWTAuth::factory()->getTTL() . ' minute',
        ]);
    }
}
