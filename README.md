# Project Name - Reminder API using Laravel

## Overview

This Laravel-based Backend API enables you to manage reminders for your projects. It provides functionality for user authentication, allowing you to create, retrieve, update, and delete reminders through a set of API endpoints.

## Authentication

The API uses token-based authentication for secure access. Tokens have a short lifespan for enhanced security:

- **POST `/api/session`**
  - Endpoint for user login.
  - Returns access_token and refresh_token.
  - Access_token is valid for 1 minute (i couldn't make it for 20 seconds exp time T_T).
  - Refresh_token is used to request a new access_token.
  
- **PUT `/api/session`**
  - Endpoint for requesting a new access_token using a refresh_token.
  
## Reminder Operations

The reminder operations are accessible through the following routes:

- **POST `/api/reminders`**
  - Create a new reminder.
  - Requires authentication using a valid access_token.
  
- **GET `/api/reminders`**
  - Retrieve all reminders.
  - Requires authentication using a valid access_token.
  
- **GET `/api/reminders/{id}`**
  - Retrieve a specific reminder by ID.
  - Requires authentication using a valid access_token.
  
- **PUT `/api/reminders/{id}`**
  - Update a specific reminder by ID.
  - Requires authentication using a valid access_token.
  
- **DELETE `/api/reminders/{id}`**
  - Delete a specific reminder by ID.
  - Requires authentication using a valid access_token.

## Token Validity

The access_token has a short validity of 1 minute to ensure heightened security. After expiration, a new token can be requested using the refresh_token provided during login.

## Getting Started

1. Clone the repository.
   ```bash
   git clone https://gitlab.com/vityamashina27/nabitu-backend
2. Install dependencies.
    ```bash
    composer install
3. Configure your database settings in the .env file.
4. Run migrations and seed the database.
    ```bash
    php artisan migrate --seed
5. Start the development server.
    ```bash
    php -S *your_ip* -t public
## Getting Started
- Ensure proper error handling on the client side to manage token expiration scenarios.
- Token expiration time can be adjusted in the authentication configuration **(config/auth.php)**.

Feel free to explore the API documentation for more details on each endpoint.
