<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ReminderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::apiResource('reminders', ReminderController::class);
    // Route::put('/reminders/{id}', [ReminderController::class, 'update']);
});

Route::put('/session', [AuthController::class, 'refresh']);
Route::post('/session', [AuthController::class, 'login']);
